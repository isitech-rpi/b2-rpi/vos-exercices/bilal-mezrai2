#include "Personnage.h"

int main() {
    using namespace Personnages;

    Personnage guethenoc("Guethenoc");
    guethenoc.afficher();


    Personnage* balcmeg = new Personnage("Balcmeg");
    balcmeg->afficher();
    delete balcmeg;

    return 0;
}
