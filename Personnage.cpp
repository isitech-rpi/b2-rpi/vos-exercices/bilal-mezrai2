#include "Personnage.h"
#include <iostream>

namespace Personnages {

    Personnage::Personnage(const std::string& nom) : nom(nom), vie(100), niveau(1) {}

    void Personnage::afficher() const {
        std::cout << "Nom: " << nom << ", Vie: " << vie << ", Niveau: " << niveau << std::endl;
    }

}
