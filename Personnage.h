#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include <string>

namespace Personnages {

    class Personnage {
    public:
        Personnage(const std::string& nom);
        void afficher() const;

    protected:
        std::string nom;
        int vie;
        int niveau;
    };

}

#endif
